#ifndef __DIRECTOR_H
#define __DIRECTOR_H

#include <vdr/plugin.h>
#include <vdr/status.h>
#include <vdr/remote.h>

static const char *VERSION        = "0.2.8";
static const char *DESCRIPTION    = "plugin to use the premiere multifeed option";
static const char *MAINMENUENTRY  = "Director";
int hide = 0;
int portalmode = 1;
int swapKeys = 0;
int displayChannelInfo = 1;
int autostart = 1;

class cDirectorStatus : public cStatus {
private:
	cPlugin* parent;
protected:
  virtual void ChannelSwitch(const cDevice *Device, int ChannelNumber);
public:
	cDirectorStatus(cPlugin* plugin);
  };

class cPluginDirector : public cPlugin {
private:	
  // Add any member variables or functions you may need here.
  cDirectorStatus *directorStatus;  
public:
  cPluginDirector(void);
  virtual ~cPluginDirector();
  virtual const char *Version(void) { return VERSION; }
  virtual const char *Description(void) { return DESCRIPTION; }
  virtual const char *CommandLineHelp(void);
  virtual bool ProcessArgs(int argc, char *argv[]);
  virtual bool Start(void);
  virtual void Housekeeping(void);
  virtual const char *MainMenuEntry(void) { return (hide ? NULL : tr(MAINMENUENTRY)); }
  virtual cOsdObject *MainMenuAction(void);
  virtual cMenuSetupPage *SetupMenu(void);
  virtual bool SetupParse(const char *Name, const char *Value);
  };

#endif //__DIRECTOR_H
