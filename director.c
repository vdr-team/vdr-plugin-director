/*
 * director.c: A plugin for the Video Disk Recorder
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id$
 */

#include "i18n.h"
#include "director.h"
#include "directorosd.h"


cDirectorStatus::cDirectorStatus(cPlugin* plugin)
{
	parent = plugin;
}
	
void cDirectorStatus::ChannelSwitch(const cDevice *Device, int ChannelNumber)
{
	const cChannel* Channel = Channels.GetByNumber(Device->CurrentChannel());
	if(Channel != NULL && Channel->LinkChannels() != NULL && Channel->LinkChannels()->Count() > 1)
		if(parent && ChannelNumber != 0)
		{
			//dont know why Channelnumber != 0 is necessary, but it is
			cRemote::CallPlugin(parent->Name());
		}
}

cPluginDirector::cPluginDirector(void)
{
  // Initialize any member variables here.
  // DON'T DO ANYTHING ELSE THAT MAY HAVE SIDE EFFECTS, REQUIRE GLOBAL
  // VDR OBJECTS TO EXIST OR PRODUCE ANY OUTPUT!
  directorStatus = NULL;
}

cPluginDirector::~cPluginDirector()
{
  // Clean up after yourself!
  delete directorStatus;
}

const char *cPluginDirector::CommandLineHelp(void)
{
  // Return a string that describes all known command line options.
  return NULL;
}

bool cPluginDirector::ProcessArgs(int argc, char *argv[])
{
  // Implement command line argument processing here if applicable.
  return true;
}

bool cPluginDirector::Start(void)
{
  // Start any background activities the plugin shall perform.
  RegisterI18n(Phrases);
  if(autostart == 1)
  	directorStatus = new cDirectorStatus(this);
  // Default values for setup
  return true;
}

void cPluginDirector::Housekeeping(void)
{
  // Perform any cleanup or other regular tasks.
}

cOsdObject *cPluginDirector::MainMenuAction(void)
{
	return new cDirectorOsd(portalmode, swapKeys, displayChannelInfo, autostart);
}

bool cPluginDirector::SetupParse(const char *Name, const char *Value)
{
	// Parse your own setup parameters and store their values.
	if (!strcasecmp(Name, "HideMenu"))
		hide = atoi(Value);
	else if (!strcasecmp(Name, "SwapKeys"))
		swapKeys = atoi(Value);
	else if (!strcasecmp(Name, "PortalMode"))
		portalmode = atoi(Value);
	else if (!strcasecmp(Name, "DisplayChannelInfo"))
		displayChannelInfo = atoi(Value);
	else if (!strcasecmp(Name, "Autostart"))
		autostart = atoi(Value);
	return true;
}

//the setup part
class cMenuSetupDirector : public cMenuSetupPage {
 public:
  cMenuSetupDirector();
 protected:
  virtual void Store(void);
 private:
 	int new_hide;
};

cMenuSetupDirector::cMenuSetupDirector()
{
	new_hide = hide;    
	Add(new cMenuEditBoolItem(tr("Hide main menu entry"), &hide, tr("no"), tr("yes")));
	Add(new cMenuEditBoolItem(tr("Swap up and down"), &swapKeys, tr("no"), tr("yes")));
	Add(new cMenuEditBoolItem(tr("Portal Mode"), &portalmode, tr("no"), tr("yes")));
	Add(new cMenuEditBoolItem(tr("Display info on channel change"), &displayChannelInfo, tr("no"), tr("yes")));
	Add(new cMenuEditBoolItem(tr("Start the plugin automatically"), &autostart, tr("no"), tr("yes")));
}

void cMenuSetupDirector::Store(void)
{
	SetupStore("HideMenu", new_hide = hide);
	SetupStore("SwapKeys", swapKeys);
	SetupStore("PortalMode", portalmode);
	SetupStore("DisplayChannelInfo", displayChannelInfo);
	SetupStore("Autostart", autostart);
}

cMenuSetupPage *cPluginDirector::SetupMenu(void)
{
	
  // Return a setup menu in case the plugin supports one.
  return new cMenuSetupDirector();
}

VDRPLUGINCREATOR(cPluginDirector); // Don't touch this!
